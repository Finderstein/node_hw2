import express from 'express';
import checkDuplicateUsername from '../middleware/verifySignUp.js';
import { signup, signin } from '../controllers/auth.controller.js';

const router = new express.Router();

router.post('/register', [checkDuplicateUsername], signup);

router.post('/login', signin);

export default router;
