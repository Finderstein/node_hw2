import mongoose from 'mongoose';

const UserShema = new mongoose.Schema(
	{
		username: { type: String, required: true },
		password: { type: String, required: true },
		createdDate: { type: String, required: true }
	},
	{ versionKey: false }
);

const UserModel = mongoose.model('User', UserShema);

class User {
	constructor(username, password, createdDate) {
		this.username = username;
		this.password = password;
		this.createdDate = createdDate;
	}

	static insert(user) {
		return new UserModel(user).save();
	}

	static getById(id) {
		return UserModel.findById({ _id: id });
	}

	static getByUsername(username) {
		return UserModel.findOne({ username: username });
	}

	static updateUser(user) {
		return UserModel.findByIdAndUpdate(user._id, user);
	}

	static delete(id) {
		return UserModel.findByIdAndDelete(id);
	}
}

export default User;
